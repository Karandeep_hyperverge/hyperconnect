import React, { createContext, useReducer } from 'react'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css';
import HyperNavbar from './components/HyperNavbar';
import Home from './components/Home';
import Feed from './components/Feed';
import Profile from './components/Profile';
import Login from './components/Login';
import Signup from './components/Signup';
import Logout from './components/Logout'
import { initialState, reducer } from './reducer/UseReducer';
export const UserContext = createContext();


function App() {

  const [state, dispatch] = useReducer(reducer, initialState)
  return (
    <>
      <UserContext.Provider value={{ state, dispatch }}>
        <HyperNavbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/feed" element={<Feed />} />
          <Route exact path="profile" element={<Profile />} />
          <Route exact path="login" element={<Login />} />
          <Route exact path="signup" element={<Signup />} />
          <Route exact path="logout" element={<Logout />} />

        </Routes>
      </UserContext.Provider>
    </>
  );
}

export default App;
