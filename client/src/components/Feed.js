import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';

import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import PostCard from './Post/Post';
import Stack from '@mui/material/Stack';
import AddPost from './Form/AddPost.js';
import EditProfile from './Form/EditProfile';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.text.secondary,
}));


const Feed = () => {

  const navigate = useNavigate();
  const [userData, setUserData] = useState({});

  const isLogedIn = async () => {
    try {
      const res = await fetch('/isLogedIn', {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        credentials: "include"
      });

      const data = await res.json();
      console.log(data);
      setUserData(data);

      if (!res.status === 200) {
        navigate('/login');
      } else {
        console.log("welcome" + data.name);
      }

    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    isLogedIn();
  }, []);

  const props_data = {
    header:{
      avatarurl: "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg",
      nameofcreator: "karandeep",
      createdat: "September 14, 2016"
    },
    body:{
      image: "https://thumbs.dreamstime.com/b/beautiful-natural-senary-our-country-143695342.jpg",
      description: "This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like."
    },
    comments:[
      {
        sender: "arshdeep",
        sender_url: "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg",
        comment: "this is a comment"
      }
    ]
  }

  return (
    <Container maxWidth="lg" sx={{ mx: 4, my: 4 }}>
      <Box sx={{ flexGrow: 1 }} >
        <Grid container spacing={2} >
          <Grid item xs={3}>
            <Item >
              <AddPost />
            </Item>
          </Grid>
          <Grid item xs={9} container justify="center">
              <Item>
                <PostCard props_data={props_data}/>
              </Item>
          </Grid>
        </Grid>
      </Box>
    </Container>
  )
}

export default Feed