import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function TextControlsExample() {
  return (
    <Form>
      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
        <Form.Label>Enter Description</Form.Label>
        <Form.Control as="textarea" rows={2} />
      </Form.Group>
      <Form.Group controlId="formFile" className="mb-3">
        <Form.Label>Choose a picture</Form.Label>
        <Form.Control type="file" />
      </Form.Group>
      <Button variant="primary" size="lg" type="submit">
        Upload
      </Button>

    </Form>
  );
}

export default TextControlsExample;