import React, { useState, useEffect, useContext } from 'react'
import { Link } from "react-router-dom"
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../App';
import Notifications from './popovers/Notifications';

const HyperNavbar = () => {


  const navigate = useNavigate();
  const [userData, setUserData] = useState({});
  const { state, dispatch } = useContext(UserContext);

  const isLogedIn = async () => {
    try {
      const res = await fetch('/isLogedIn', {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        credentials: "include"
      });

      const data = await res.json();
      console.log(data);
      setUserData(data);

      if (!res.status === 200) {
        navigate('/login');
      } else {
        console.log("welcome" + data.name);
      }

    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    isLogedIn();
  }, []);

  if (state) {
    return (
      <Navbar bg="light" variant="light">
        <Container>
          <Navbar.Brand><Link to="/">Hyper Connect</Link></Navbar.Brand>
          <Nav className="ml-auto">
            <Nav.Link ><Link to="/feed">Hyper Connect</Link></Nav.Link>
            <Nav.Link ><Link to="/profile">Profie</Link></Nav.Link>
            <Nav.Link ><Notifications /></Nav.Link>
            <Nav.Link ><Link to="/logout">Logout</Link></Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    )
  } else {
    return (
      <Navbar bg="light" variant="light">
        <Container>
          <Navbar.Brand><Link to="/">Hyper Connect</Link></Navbar.Brand>
          <Nav className="ml-auto">
            <Nav.Link ><Link to="/login">Login</Link></Nav.Link>
            <Nav.Link ><Link to="/signup">Signup</Link></Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    )
  }

}

export default HyperNavbar