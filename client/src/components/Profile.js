import React from 'react'
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Profilecard from "./profile/Profilecard.js"
import Friendcard from './profile/Friendcard.js';
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.text.secondary,
}));

const Profile = () => {
  return (
    <Container maxWidth="lg" sx={{ mx: 4, my: 4 }}>
      <Box sx={{ flexGrow: 1 }} >
        <Grid container spacing={2} >
          <Grid item xs={4}>
            <Item >
              <Profilecard />
            </Item>
            <Item sx={{ my: 4 }}>
              <Friendcard />
            </Item>
          </Grid>
          <Grid item xs={8}>
            <Item >
              <h2>Section 2</h2>

            </Item>
          </Grid>

        </Grid>
      </Box>
    </Container>
  )
}

export default Profile