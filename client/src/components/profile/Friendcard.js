import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import ImageIcon from '@mui/icons-material/Image';
import WorkIcon from '@mui/icons-material/Work';
import BeachAccessIcon from '@mui/icons-material/BeachAccess';

export default function Friendcard(props) {
  return (
    <>
    <h4>Friends</h4>
    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>

      <ListItem>
        <ListItemAvatar>
          <Avatar src={'https://expertphotography.b-cdn.net/wp-content/uploads/2020/08/profile-photos-4.jpg'}/>
        </ListItemAvatar>
        <ListItemText primary="Photos"/>
      </ListItem>

    </List>
    </>

  );
}