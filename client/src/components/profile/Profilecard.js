import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import SkipNextIcon from '@mui/icons-material/SkipNext';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function Profilecard() {
  const theme = useTheme();

  return (
    <Card sx={{ display: 'flex' }}>
      <Box sx={{ display: 'flex', flexDirection: 'column' }}>

        <CardContent sx={{ flex: '1 0 auto' }}>
          <Typography component="div" variant="h5">
            Karandeep Singh
          </Typography>
          <Typography variant="subtitle1" color="text.secondary" component="div">
            karan@gmail.com
          </Typography>
        </CardContent>

        <Form className='mx-2 my-2'>
        <Form.Group controlId="formFile" className="mb-3">
          <Form.Control type="file" size='sm' />
        </Form.Group>
        <Button variant="primary" size="sm" type="submit">
          Update profile
        </Button>

      </Form>

      </Box>

      <CardMedia
        component="img"
        sx={{ width: 151 }}
        image="https://st4.depositphotos.com/4157265/41100/i/450/depositphotos_411005388-stock-photo-profile-picture-of-smiling-30s.jpg"
        alt="Live from space album cover"
      />
      
      
    </Card>

  );
}