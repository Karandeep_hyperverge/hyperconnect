import PostSchema from "../models/post.js";
import UserSchema from "../models/user.js";

function flatten(into, node){
    if(node == null) return into;
    if(Array.isArray(node)) return node.reduce(flatten, into);
    into.push(node);
    return flatten(into, node.children);
}


export const getPost = async (req, res) => {
    const {userID} = req.body;
    try {
        const data = await UserSchema.findById(userID);
        const friends = data.friends;
        var postData =[]
        for (var friend in friends) {
            // console.log(friends[friend]);
            const friendPostData=await PostSchema.find({creator: friends[friend]}).sort({createdAt: -1});
            postData.push(friendPostData);
        }
        // console.log();
        var out = flatten([], postData);

        // console.log(out)
        res.status(200).send(out);

    } catch (error) {
        res.status(400).json({error:error.message});
    }
}

export const getUsersPost = async (req, res) => {
    const { creator } = req.body;
    try {
        const data = await PostSchema.find({ creator: creator });
        if (data) {
            res.status(200).send(JSON.stringify(data));
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ error: error.message })
    }
}

export const addPost = async (req, res) => {
    const { creator, image, description } = req.body;

    try {
        const newPost = new PostSchema({ creator, image, description });
        const newPostAdded = await newPost.save();
        console.log(newPostAdded)
        if (newPostAdded) {
            console.log("new post added");
            res.status(200).json({ message: "new post added" });
        }

    } catch (error) {
        console.log(error);
        res.status(400).json({ message: error.message })
    }

}

export const deletePOST = async (req, res) => {
    const { creator, postid } = req.body;
    try {
        const response = await PostSchema.deleteOne({ creator: creator, _id: postid });
        if (response) {
            res.status(200).json({ message: "Post deleted" });
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: error.message });
    }
}

export const likePost = async (req, res) => {
    const { creator, postid } = req.body;
    try {
        const filter = { creator: creator, _id: postid };
        const response = await PostSchema.findOneAndUpdate(filter, { $inc: { likes: 1 }, $push: { likedby: creator } });
        if (response) {
            res.status(200).json({ message: "Post Updated" });
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: error.message });
    }
}

export const commentPost = async (req, res) => {
    const { creator, postid, description } = req.body;
    try {
        const response = await PostSchema.findOneAndUpdate({ _id: postid }, { $push: { comments: { user: creator, comment: description } } });
        if (response) {
            res.status(200).json({ message: "Comment added" });
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: error.message });
    }

}

export const sharePost = async (req, res) => {
    const { sender, postid, receiver } = req.body;

    try {
        const response = await PostSchema.findOneAndUpdate({ _id: postid }, { $push: { shared: { by: sender, to: receiver } } });
        if (response) {
            res.status(200).json({ message: "Post Shared" });
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: error.message });
    }

}