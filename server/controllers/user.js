import UserSchema from "../models/user.js";

export const getUserFriends = async (req, res) => {
    const {userID} = req.body;
    try {
        const data = await UserSchema.findById(userID);
        const friends = data.friends;
        //Find all post related to friends and in sorted manner

        res.status(200).send(friends);

    } catch (error) {
        res.status(400).json({error:error.message});
    }
}

export const addUserFriends = async (req, res) => {
    const {userID,friendID} = req.body;
    try {
        const data = await UserSchema.findOneAndUpdate({_id:userID},{ $push: {friends: friendID}});
        const friends = data.friends;
        res.status(200).send(friends);

    } catch (error) {
        res.status(400).json({error:error.message});
    }
}

export const removeUserFriends = async (req, res) => {
    const {userID,friendID} = req.body;
    try {
        const data = await UserSchema.findOneAndUpdate({_id:userID},{ $push: {friends: friendID}});
        const friends = data.friends;
        res.status(200).send(friends);

    } catch (error) {
        res.status(400).json({error:error.message});
    }
}