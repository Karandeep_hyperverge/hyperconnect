import mongoose, { mongo } from "mongoose";
const notificationSchema = mongoose.Schema({
    sender: {
        type: String,
        required: true
    },
    receiver: {
        type: String,
        required: true
    },
    comment: {
        type: Number
    }
});

const NotificationSchema = mongoose.model('NotificationSchema', notificationSchema);

export default NotificationSchema;