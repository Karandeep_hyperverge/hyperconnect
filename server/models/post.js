import mongoose, { mongo } from "mongoose";
const postSchema = mongoose.Schema({
    creator: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    likes: {
        type: Number,
        required: true,
        default: 0
    },
    description: {
        type: String,
    }, comments: {
        type: [{ user: String, comment: String }]
    },
    shared: {
        type: [{ by: String, to: String }]
    },
    createdAt: {
        type: Date,
        default: new Date()
    },likedby:{
        type: [String]
    }
});

const PostSchema = mongoose.model('PostSchema', postSchema);

export default PostSchema;