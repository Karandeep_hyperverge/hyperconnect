import express from "express";
import { getPost, addPost, deletePOST, likePost,commentPost, sharePost, getUsersPost } from '../controllers/post.js'
import Authenticate from '../middleware/authenticate.js'
const router = express.Router();

//get methods
router.get('/getpost',getPost)
router.get('/getuserspost',getUsersPost)

//Post methods
router.post('/addpost', addPost);
router.post('/deletepost', deletePOST);
router.post('/like', likePost);
router.post('/comment', commentPost);
router.post('/share', sharePost);



export default router;