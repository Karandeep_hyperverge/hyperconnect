import express from "express";
import { getUserFriends, addUserFriends, removeUserFriends } from '../controllers/user.js'
import Authenticate from '../middleware/authenticate.js'
const router = express.Router();

//get methods
router.get('/getusersfriends',getUserFriends)

//Post methods
router.post('/adduserfriends', addUserFriends);
router.post('/removeuserfriends', removeUserFriends);


export default router;